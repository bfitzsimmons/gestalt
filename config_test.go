package gestalt

import (
	"log"
	"os"
	"runtime"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type configuration struct {
	Debug          bool    `gestalt:"debug"`
	Port           int     `gestalt:"port"`
	GOMAXPROCS     int     `gestalt:"GOMAXPROCS"`
	SecretKey      string  `gestalt:"secret_key"`
	SecretPassword string  `gestalt:"secret_password"`
	MaxInt         int     `gestalt:"max_num"`
	MaxFloat       float64 `gestalt:"max_float"`
	MemcachedHosts string  `gestalt:"memcached_hosts"`
	Version        string  `gestalt:"version"`
}

func TestProcessConfig(t *testing.T) {
	var err error

	os.Clearenv()

	// Defaults.
	Config := configuration{
		Port:           8080,
		GOMAXPROCS:     runtime.NumCPU(),
		MemcachedHosts: "localhost:11211",
		Version:        "0.1.0",
	}

	if err = os.Setenv("FOO_DEBUG", "false"); err != nil {
		Println(err)
	}

	if err = os.Setenv("FOO_SECRET_KEY", "29823HF982H39FH298F923H"); err != nil {
		Println(err)
	}

	if err = os.Setenv("FOO_SECRET_PASSWORD", "super secret password"); err != nil {
		Println(err)
	}

	if err = os.Setenv("FOO_MAX_NUM", "100"); err != nil {
		Println(err)
	}

	if err = os.Setenv("FOO_MAX_FLOAT", "100.5"); err != nil {
		Println(err)
	}

	Convey("When setting the config. settings there are no unexpected errors", t, func() {
		if err := Process("foo", &Config); err != nil {
			log.Fatal(err.Error())
		}

		So(Config.Debug, ShouldEqual, false)
		So(Config.Port, ShouldEqual, 8080)
		So(Config.GOMAXPROCS, ShouldEqual, runtime.NumCPU())
		So(Config.SecretKey, ShouldEqual, "29823HF982H39FH298F923H")
		So(Config.SecretPassword, ShouldEqual, "super secret password")
		So(Config.MaxInt, ShouldEqual, 100)
		So(Config.MaxFloat, ShouldEqual, 100.5)
		So(Config.MemcachedHosts, ShouldEqual, "localhost:11211")
		So(Config.Version, ShouldEqual, "0.1.0")
	})
}
