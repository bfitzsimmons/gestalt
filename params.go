package gestalt

import (
	"errors"
	"fmt"
	"net/url"
	"reflect"
	"strconv"
	"strings"

	"bitbucket.org/bfitzsimmons/gadgets"
	"github.com/fatih/structs"
)

// Errors.
var (
	ErrInvalidQueryParam = errors.New("You have provided an invalid query param.")
)

// ProcessParams takes the environment variables and populates the passed struct.
func ProcessParams(params url.Values, paramStore interface{}) (err error) {
	s := structs.New(paramStore)

	allowedParams := []string{}

	for i, field := range s.Fields() {
		var err error

		// Get the reflection field.
		f := reflect.ValueOf(paramStore).Elem().Field(i)

		// Get the field tag.
		tagParts := strings.Split(field.Tag("gestalt"), ",")
		name := tagParts[0]
		allowedParams = append(allowedParams, name)
		required := true
		if len(tagParts) > 1 {
			required = false
		}

		// Grab the param.
		value := params.Get(name)

		if value == "" {
			// Is there a default value?
			if field.IsZero() && required {
				return fmt.Errorf("%s must be set", name)
			}
			continue
		}

		switch field.Kind() {
		case reflect.String:
			if err = field.Set(value); err != nil {
				return err
			}

		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			intValue, err := strconv.ParseInt(value, 0, f.Type().Bits())
			if err != nil {
				return fmt.Errorf("Error converting %s: %v", name, err)
			}

			f.SetInt(intValue)

		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			uintValue, err := strconv.ParseUint(value, 0, f.Type().Bits())
			if err != nil {
				return fmt.Errorf("Error converting %s: %v", name, err)
			}

			f.SetUint(uintValue)

		case reflect.Float64:
			floatValue, err := strconv.ParseFloat(value, f.Type().Bits())
			if err != nil {
				return fmt.Errorf("Error converting %s: %v", name, err)
			}

			if err = field.Set(floatValue); err != nil {
				return err
			}

		case reflect.Bool:
			boolValue, err := strconv.ParseBool(value)
			if err != nil {
				return fmt.Errorf("Error converting %s: %v", name, err)
			}

			if err = field.Set(boolValue); err != nil {
				return err
			}
		}
	}

	// Test for any extra query params.
	if err := CheckParams(params, allowedParams); err != nil {
		return err
	}

	return nil
}

// CheckParams compares the passed params. against the list of allowed params.
func CheckParams(params url.Values, allowedParams []string) (err error) {
	for key := range params {
		if !gadgets.StringListContains(key, allowedParams) {
			return ErrInvalidQueryParam
		}
	}
	return err
}
