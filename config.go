package gestalt

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"

	"github.com/fatih/structs"
)

// ProcessConfig takes the environment variables and populates the passed struct.
func ProcessConfig(prefix string, config interface{}) (err error) {
	var nameParts []string

	s := structs.New(config)

	for i, field := range s.Fields() {
		// Get the reflection field.
		f := reflect.ValueOf(config).Elem().Field(i)

		// Get the field tag.
		name := field.Tag("gestalt")

		// Is it required?
		if strings.Contains(name, ",") {
			nameParts = strings.Split(name, ",")
			name = nameParts[0]
		}

		// Grab the env. variable.
		key := strings.ToUpper(fmt.Sprintf("%s_%s", prefix, name))
		value := os.Getenv(key)

		if value == "" {
			// Is there a default value?
			if field.IsZero() {
				// Is it allowed to be empty?
				if nameParts != nil && nameParts[len(nameParts)-1] == "omitempty" {
					continue
				}

				return fmt.Errorf("%s must be set", key)
			}
			continue
		}

		switch field.Kind() {
		case reflect.String:
			if err = field.Set(value); err != nil {
				return err
			}
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			intValue, err := strconv.ParseInt(value, 0, f.Type().Bits())
			if err != nil {
				return fmt.Errorf("Error converting %s: %v", key, err)
			}

			f.SetInt(intValue)
		case reflect.Bool:
			boolValue, err := strconv.ParseBool(value)
			if err != nil {
				return fmt.Errorf("Error converting %s: %v", key, err)
			}

			if err = field.Set(boolValue); err != nil {
				return err
			}
		case reflect.Float64:
			floatValue, err := strconv.ParseFloat(value, f.Type().Bits())
			if err != nil {
				return fmt.Errorf("Error converting %s: %v", key, err)
			}

			if err = field.Set(floatValue); err != nil {
				return err
			}
		}
	}

	return err
}

func ConstructHosts(hostString string, port int) string {
	hostSlice := []string{}
	for _, host := range strings.Split(hostString, ",") {
		host := fmt.Sprintf("%s:%d", host, port)
		hostSlice = append(hostSlice, host)
	}

	return strings.Join(hostSlice, ",")
}
